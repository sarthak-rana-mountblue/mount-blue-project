# Basic datatypes and  data structures with code samples.

Here I am going to talk about the basic datatypes and data structures. Some people are confused about what are datatypes and data structures. First I will cover the datatypes and then  go to the data structures.

# What is Datatype
### Datatype can be easily understood by the name itself, which is, a type of data. To understand correctly think of making a list of your favorite actor’s name, age, house number, ranking, a famous movie, height, weight, and so on.
 
### Now watch carefully and tell that whether that value is an integer number, floating-point number, character, array of characters, or boolean.
 
### Don’t know what these are, don’t worry, let’s learn. There are 8 primitive data types in Java:
 
 
## Integer number: 
### Integer numbers are like: -4, -3, -2, -1, 0, 1, 2, 3, 4. These numbers do not contain any decimal value.
 
Following syntax is used to declare an integer value in java
```java
byte b = 32;
short s = 3243;
int a = 21;
long l = 68672462L;
```
### The following table describes each way of storing integer values:



|Data Type|Size|Description|
|-|-|-|
|byte|1 byte|Stores numbers from -128 to 127|
|short|2 bytes|Stores numbers from -32,768 to 32767|
|int|4 bytes|Stores numbers from -2,147,483,648 to 2,147,483,647|
|long|8 bytes|Stores numbers from -9223372036854775808 to 9223372036854775807|



## Floating-point number:
### Floating-point numbers are like: 2.13, 0.00320, -725.009, 5.8, 21.2. Floating points contains decimal values. 
Following syntax is used to declare a decimal value in java
```java
float a = 21f;
//’f’ should be appended at the end of the float value 
 
double d = 2432.1323;
//the main difference between float and decimal is that double can store more values than float do
```
 
### The following table describes each way of storing floating-point values:

|Data Type|Size|Description|
|-|-|-|
|float|4 bytes|Stores fractional numbers. Sufficient for storing 6 to 7 decimal digits|
|double|8 bytes|Stores fractional numbers. Sufficient for storing 15 decimal digits|
 
	
## Character: 
### Characters are simply alphabets like ‘a’, ‘b’, ‘f’, etc. Did you notice this(') symbol this is used to define a character value
 
```java
char ch = ‘k’;
//this is the way to declare a character value in java
```
 
|Data Type|Size|Description|
|-|-|-|
|char|2 bytes|Stores a single character/letter or ASCII values
 
## Boolean: 
### If you want to save some data in either on/off state then boolean is the best choice
 
```java
boolean b = true;
//or
boolean b = false;
```
### You can save that someone is married or unmarried in a boolean variable. Boolean is the smallest data type.
 
|Data Type|Size|Description|
|-|-|-|
|boolean|1 bit|Stores true or false value
#
>### There is also one important data type without which we cannot continue and that data type is String. String is not a primitive data type in java but most of the time we use it in our code. String is a collection of characters or an array of characters.
.   .       .          .         .         .       .
.       .         .        .       .         .       .       
.           .            .              .             .     
.            .

### Now can you tell which value comes in which data type?
>The following table show some values with 
>|Value|Data Type
>|-|-|
>454|May be short, int, long but not byte, char, boolean, float, double
>32.12|May be float, double but not int, long, byte, short, char, boolean
>’s’|This is char
>true or false| boolean
>”Amitabh Bachchan”|String (collection of characters)
#

# Data structure 
### Data is always stored in some format like you can store the data in your notebook as a set of paragraph or you can also save the data as a list of some items or like a table consisting of rows and columns.

### A data structure is a data organization, management, and storage format that enables efficient access and modification.

### There are lots of data structures present out there and they are also present in java.

## Some commonly used data structures are :-
- Arrays
- Stacks
- Queues
- Linked Lists
- Trees
- Graphs

### All the above listed data structures are inbuild in java and are present in java.util package 

``` java
import java.util.*;
//This package has all you need to use data structures
```

### Data structures make you work very easy and also make it optimized
